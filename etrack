#!/bin/sh

# Copyright (C) 2001-2009, 2012, 2013 Thien-Thi Nguyen
# This file is part of ETRACK, released under GNU GPL with
# ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

##
# Usage: etrack [-nw] [-c CONFIG]          # use emacs interface
#        etrack --display-example-config   # to stdout
#        etrack --shell                    # for shell interface
#        etrack -b --help                  # for more help
#
# Option "-nw" means to run Emacs in terminal mode (no X).
# Env var EMACS specifies which Emacs to use (default "emacs").
# If "-c CONFIG" is omitted you must set env var
# ETRACK_CONFIG to be the name of the config file.
##

if [ x"$1" = x--help ] ; then
    sed '/^##/,/^##/!d;/^##/d;s/^# //g;s/^#$//g' $0
    v=$ETRACK_CONFIG
    if [ x"$v" = x ] ; then echo '(This is currently not set.)'
    else echo "(Current value: $v)" ; fi
    exit 0
fi

at=`dirname $0`
at=`cd "$at" && pwd`
if [ -f "$at/etrack.el" ]
then datadir="$at"
else datadir='|pkg-datadir|'
fi

if [ x"$1" = x--version ] ; then
    sed '/^-/!d;s//etrack (ETRACK)/;s/ | .*//;q' "$datadir/NEWS"
    exit 0
fi

if [ x"$1" = x--display-example-config ] ; then
    cat "$datadir/example.etrack.conf"
    exit 0
fi

if [ x"$1" = x-nw ] ; then
    nw="$1" ; shift
fi

if [ x"$1" = x-c ] ; then
    ETRACK_CONFIG=$2
    export ETRACK_CONFIG
    shift
    shift
fi

if [ x"$ETRACK_CONFIG" = x ] ; then
    echo ERROR: Need to set env var ETRACK_CONFIG.
    exit 1
fi

if [ x"$1" = x--shell ] ; then
    set -- '-b' 'shell'
fi

if [ x"$1" = x-b ] ; then               # backend
    shift
    exec guile -s "$datadir/etrack-be" "$@"
fi

test "$EMACS" = t && unset EMACS
${EMACS-emacs} -q $nw \
      -l "$datadir/etrack.el" \
      -f etrack-kill-window-system-crap \
      -f etrack

# etrack ends here
