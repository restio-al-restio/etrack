#!/bin/sh
#
# Copyright (C) 2002-2009, 2011-2013 Thien-Thi Nguyen
# This file is part of ETRACK, released under GNU GPL with
# ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

set -e

guile-baux-tool snuggle m4 build-aux/
guile-baux-tool import punify gbaux-do

autoreconf --verbose --install --symlink -I build-aux

# These override what ‘autoreconf --install’ creates.
# Another way is to use gnulib's config/srclist-update.
actually ()
{
    gnulib-tool --copy-file $1 $2
}
actually doc/INSTALL.UTF-8 INSTALL
actually build-aux/install-sh

# Decruft, for now (maybe gnulib-tool will have an option to DTRT someday).
rm -vf INSTALL~ build-aux/install-sh~

# autogen.sh ends here
