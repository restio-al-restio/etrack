;;; about.scm --- display some info about ETRACK

;; Copyright (C) 2004-2009, 2011-2013 Thien-Thi Nguyen
;; This file is part of ETRACK, released under GNU GPL with
;; ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

(define-command (about)                 ; init=#f
  (flush-all-ports)
  (system (fs "cat ~A" (datafile "NEWS")))
  (flush-all-ports))

;;; about.scm ends here
