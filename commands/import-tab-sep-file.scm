;;; import-tab-sep-file.scm --- import tab-separated data from FILE

;; Copyright (C) 2004-2009, 2012, 2013 Thien-Thi Nguyen
;; This file is part of ETRACK, released under GNU GPL with
;; ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

(use-modules
 ((ttn-do mogrify) #:select (find-file-read-only
                             editing-buffer))
 ((srfi srfi-13) #:select (string-trim-both)))

(define-command (import-tab-sep-file file) ; init=#t
  (and (symbol? file) (set! file (symbol->string file)))
  (let* ((buf (or (false-if-exception (find-file-read-only file))
                  (error "invalid file")))
         (/tab/ (split-on-proc #\tab))
         (/comma/ (split-on-proc #\,)))
    ;; todo: separate into two phases (validate / insert)
    (FE (editing-buffer buf
          (buffer-lines))
        (lambda (line)
          (let ((toks (/tab/ line)))
            (cond ((string-null? line))
                  ((= 4 (length toks))
                   (apply
                    (lambda (date float-amount ac raw-details)
                      (let ((data (list date
                                        (ex-tr
                                         (* 100 (string->number
                                                 float-amount)))
                                        ac
                                        (map string-trim-both
                                             (/comma/ raw-details)))))
                        (fso "insert: ~S => " data)
                        (process-command `(insert ,data))))
                    toks))
                  (else (fso "unrecognized: ~A\n" line))))))))

;;; import-tab-sep-file.scm ends here
