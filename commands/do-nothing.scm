;;; do-nothing.scm --- do absolutely nothing at all, truthfully

;; Copyright (C) 2004-2009, 2013 Thien-Thi Nguyen
;; This file is part of ETRACK, released under GNU GPL with
;; ABSOLUTELY NO WARRANTY.  See the file COPYING for details.

(define-command (do-nothing)            ; init=#f
  #t)

;;; do-nothing.scm ends here
